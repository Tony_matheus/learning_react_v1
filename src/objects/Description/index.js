import styled from "styled-components";
import { center } from "../../styles/tools"

export const Description = styled.p`
  font-size: 18px;
  width: 270px;
  ${center}; 
  margin-bottom: var(--spacing-large);
`;

export default Description;