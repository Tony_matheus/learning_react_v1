import styled from "styled-components";

const Price = styled.h2`
  font-size: 100px;
  margin-bottom: var(--spacing-medium);
`;

export default Price;