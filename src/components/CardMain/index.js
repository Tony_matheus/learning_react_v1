import React from 'react';

import Name from "../../objects/Name";
import Price from "../../objects/Price";
import Description from "../../objects/Description";

import Button from '../../components/Button'

import Card from './styles';

const CardMain = (props) => (
    <Card>
        <Name>{props.name}</Name>
        <Price>{props.price}</Price>
        <Description>{props.description}</Description>
        <Button primary >Buy </Button>
    </Card>
);

export default CardMain;