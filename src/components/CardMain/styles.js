import styled from "styled-components";
import {center} from "../../styles/tools";

const Card = styled.article` 
    font-family: "Open Sans", sans-serif;
    background-color: var(--color-first);
    color: var(--color-zero);
    padding-top: 58px;
    padding-bottom: 40px;
    text-align: center;
    ${center};
    width: 370px;
    margin-top: 10px;
`;

export default Card;