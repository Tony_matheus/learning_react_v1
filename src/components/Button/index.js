import styled from "styled-components";

export const Button = styled.a`
    display: inline-block;
    width: 120px;
    height: 35px;
    line-height: 35px;
    background-color: #fff;
    color: var(--color-first);
    border-radius: 4px;
    cursor: pointer;
    transition: all 200ms linear;
    border: 1px solid var(--color-zero);
    
    ${ props =>
    props.primary ? `&:hover {
            background-color: var(--color-first);
            color: var(--color-zero);
            border: 1px solid var(--color-zero);
        }` : `
            color: #838383;
            background-color: #ccc;
        `
    };  
`;

export default Button;