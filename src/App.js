import React, {Component, Fragment} from 'react';

import CardMain from './components/CardMain'
import axios from "axios";


class App extends Component {
    state = {
        courses: []
    };

    componentDidMount() {
        axios.get('');
    }

    render() {
        return (
            <Fragment>
                {this.state.courses.map(course => (
                    <CardMain {...course}/>
                ))}
                /* this is equal*/
                {/*{this.state.courses.map(({name, price, description}) =>{*/}
                {/*//const   = course; // destructuring*/}
                {/*return (*/}
                {/*<CardMain*/}
                {/*name={ name }*/}
                {/*price={ price }*/}
                {/*description={ description }*/}
                {/*/>*/}
                {/*)*/}
                {/*})}*/}
            </Fragment>
        )
    }
}

export default App;