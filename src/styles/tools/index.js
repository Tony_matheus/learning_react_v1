export const center = () => `
    margin-left: auto;
    margin-right: auto;
    box-sizing: border-box;
`;