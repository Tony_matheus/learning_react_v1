import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle `
  :root{
    --color-zero: #fff;
    --color-first: #3b86ff;
    
  }
`;

export default GlobalStyles;